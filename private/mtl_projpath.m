function p = mtl_projpath()
%% MTL_PROJPATH returns this project's paths for MATLAB search path
%
% Outputs:
%
%   P                   1xN cell array of paths needed to be added to MATLAB's
%                       search path



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-03-15
% Changelog:
%   2022-03-15
%       * Initial release



%% Build all paths

% Base directory
b = fullfile(fileparts(mfilename('fullpath')), '..');

% Parsed base directory
chPath = char(java.io.File(b).getCanonicalPath());

% All paths to add
p = { ...
    genpath(fullfile(chPath, 'mex')) ...
  , genpath(fullfile(chPath, 'src')) ...
};

% Porcess all paths to be absolute, fully qualified paths
p = normalizepath(p{:});


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header
