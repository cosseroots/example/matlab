function npendulum_gravitySwinging(n)
%% NPENDULUM_GRAVITYSWINGING Simulate dynamics of a rigid N-Pendulum under gravity
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% NPENDULUM_GRAVITYSWINGING()
% NPENDULUM_GRAVITYSWINGING(N)
narginchk(0, 1);

% NPENDULUM_GRAVITYSWINGING()
if nargin < 1 || isempty(n)
  n = 2;
end

% Limit number of elements to the range of [2,10]
n = limit(n, 2, 10);



%% Setup

% Create engine
engine = SimulationEngine( ...
    'Environment'   , npendulum.Environment() ...
  , 'Tree'          , SMMSFactory.NPendulum(n) ...
  , 'TimeSpan'      , [ 0.0 , 10.0 ] ...
  , 'StepSize'      , 10 * 1e-3 ...
  , 'Solver'        , NewmarkBetaSolver( ...
      'Corrector', VectorSpaceCorrector() ...
    , 'Predictor', VectorSpacePredictor() ...
  ) ...
);

% Set gravity to a realistic value
engine.SMMSTree.Gravity = [ 0.0 ; 0.0 ; -9.81 ];



%% Simulation

% Initial configuration
q0     = configurationHome(engine.SMMSTree);
qdot0  = zeros(engine.SMMSTree.VelocityNumber, 1);
qddot0 = zeros(engine.SMMSTree.VelocityNumber, 1);

% Change regular 6x6 damping matrix, the reduced damping matrix will be
% automatically calculated in the `setup` call
% engine.SMMSTree.Joint{1}.DampingMatrix = 100 * 1e-3 * engine.SMMSTree.Joint{1}.HookeanMatrix;

% Simulate
[t, q, qdot, qddot] = simulate(engine, q0, qdot0, qddot0);



%% Animation

% Prepare an axes for animation
hf = figure();
hax = axes('Parent', hf);

% And animate
animate( ...
    engine.SMMSTree ...
  , hax ...
  , t, q ...
  , 'StartFcn', @(cax, t, varargin) startFcn(engine.SMMSTree, cax, t, varargin{:}) ...
);


end


function startFcn(rob, cax, t, varargin)
%% STARTFCN


viz.style.smms(cax);

n = rob.NumBody;

cax.XAxis.Limits = [ -1.00 , +1.00 ] * n * 1.05;
cax.YAxis.Limits = [ -0.25 , +0.25 ];
cax.ZAxis.Limits = [ -1.00 , +1.00 ] * n * 1.05;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
