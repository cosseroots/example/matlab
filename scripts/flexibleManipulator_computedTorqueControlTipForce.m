function flexibleManipulator_computedTorqueControlTipForce()
%% FLEXIBLEMANIPULATOR_COMPUTEDTORQUECONTROLTIPFORCE Simulate computed torque control of a flexible manipulator actuated with a TCP force
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Setup

% SMMS Tree object
rod = manipulator.Rod();
rob = SMMSFactory.FlexibleManipulator( ...
    'Joint', rod ...
);
% Setup robot object i.e., calculate dependent properties
setup(rob);
% Disable gravity for the sake of simplicity
disableGravity(rob);

% Initial and target shape for computed torque control
q0 = configurationHome(rob);
qT = pi / rod.Length * configurationRand(rob);

% Selector vector to only select fundamental shape modes from the desired shape
% qT
B = eye(rod.PositionNumber, rod.PositionNumber);
B(:,0 < mod((1:rod.PositionNumber) - 1, rod.PositionNumber / rod.StrainNumber)) = 0;
% Select fundamental modes from desired shape
qT = B * qT;

% PID controller on flexible joint's coordinates
PID_q = PIDControl( ...
    'Kp', 27.0 ...
  , 'Ki', 45.0 ...
  , 'Kd',  9.0 ...
  , 'VelocityInput', true ...
);

% General simulation parametrization
h = 10 * 1e-3;
TActivate   = 1.0;
TTransition = 5.0;
TSim        = 5 * TTransition + TActivate;

% Environment object to provide external forces
env = manipulator.ManipulatorTCPForceEnvironment( ...
    'SMMSTree'    , rob ...
  , 'TActivate'   , TActivate  ....
  , 'TTransition' , TTransition ...
  , 'QD'          , [ q0 , qT ] ...
  , 'PID_q'       , PID_q ...
);

% Simulation engine
engine = SimulationEngine( ...
    'Environment'   , env ...
  , 'SMMSTree'      , rob ...
  , 'TimeSpan'      , [ 0.0 , TSim ] ...
  , 'StepSize'      , h ...
  , 'Solver'        , NewmarkBetaSolver( ...
      'Corrector', VectorSpaceCorrector() ...
    , 'Predictor', VectorSpacePredictor() ...
  ) ...
);



%% Simulation

% Initial configuration
q0     = env.QD(:,1);
qdot0  = zeros(engine.SMMSTree.VelocityNumber, 1);
qddot0 = zeros(engine.SMMSTree.VelocityNumber, 1);

% Change regular 6x6 damping matrix, the reduced damping matrix will be
% automatically calculated in the `setup` call
% engine.SMMSTree.Joint{1}.DampingMatrix = 100 * 1e-3 * engine.SMMSTree.Joint{1}.HookeanMatrix;

% Simulate
[t, q, qdot, qddot] = simulate(engine, q0, qdot0, qddot0);



%% Animation

% Prepare an axes for animation
hf = figure();
hl = tiledlayout( ...
    hf ...
  , 3, 4 ...
  , 'Padding'     , 'tight' ...
  , 'TileSpacing' , 'tight' ...
);


%%% Joint tracking errors
% hax = nexttile(hl, 1);
% % Tracking error vs. time
% plot(hax, t, env.Signals.Data.eq);
% % Vertical lines to indicate when controller was active
% vline(hax, [ env.TActivate , env.TActivate + env.TTransition ]);
% viz.style.timeseries(hax);
% viewtight(hax);
% title(hax, 'Joint errors');
h = smms.graphics.JointPositionChart(hl, 'Time', t, 'Q', permute(env.Signals.Data.eq, [2, 1, 3]));
h.Layout.Tile = 1;

%%% Joint state
h = smms.graphics.JointStateChartContainer(hl, 'Time', t, 'Q', permute(q, [2, 1, 3]), 'Qdot', permute(qdot, [2, 1, 3]));
h.Layout.Tile = 5;
h.Layout.TileSpan = [2, 1];

% hax = nexttile(hl, 5);
% hold(hax, 'on');
% % Joint positions vs. time
% plot(hax, t, q);
% vline(hax, [ env.TActivate , env.TActivate + env.TTransition ]);
% viz.style.timeseries(hax);
% viewtight(hax);
% title(hax, 'Joint positions');
% xlabel(hax, '');
% 
% %%% Joint velocities
% hax = nexttile(hl, 9);
% hold(hax, 'on');
% % Joint velocities vs. time
% plot(hax, t, qdot);
% vline(hax, [ env.TActivate , env.TActivate + env.TTransition ]);
% viz.style.timeseries(hax);
% viewtight(hax);
% title(hax, 'Joint velocities');
% xlabel(hax, '');

% %%% Joint accelerations
% hax = nexttile(hl, 9);
% % Joint accelerations vs. time
% plot(hax, t, qddot);
% vline(hax, [ env.TActivate , env.TActivate + env.TTransition ]);
% viz.style.timeseries(hax);
% title(hax, 'Joint accelerations');

keyboard();

% Axes for animation of robot, then animate the robot into the axes
hax = nexttile(hl, 2, [3, 3]);
animate( ...
    engine.SMMSTree ...
  , hax ...
  , t, q ...
  , 'StartFcn', @(cax, t, varargin) startFcn(engine.SMMSTree, cax, env.QD(:,2), t, varargin{:}) ...
);


end


function viewtight(ax)
%% VIEWTIGHT



if nargin < 1 || isempty(ax)
  ax = gca();
end

hq = findobj(ax, 'type', 'quiver');
if ~isempty(hq)
  set(hq, 'Visible', 'off');
end

axis(ax, 'tight');
axis(ax, 'padded');
drawnow();

ax.XAxis.LimitsMode = 'manual';
ax.YAxis.LimitsMode = 'manual';
ax.ZAxis.LimitsMode = 'manual';

if ~isempty(hq)
  set(hq, 'Visible', 'on');
end

end


function vline(varargin)
%% VLINE


[hax, args] = axescheck(varargin{:});
v = args{1};
args(1) = [];

xline( ...
    hax ...
  , v ...
  , 'LineStyle'   , '--' ...
  , 'LineWidth'   , 1.5 ...
  , 'Color'       , color('DarkGray') ...
  , 'XLimInclude' , 'off' ...
  , 'YLimInclude' , 'off' ...
  , 'ZLimInclude' , 'off' ...
  , args{:} ...
);

end


function startFcn(rob, cax, qd, t, varargin)
%% STARTFCN


viz.style.smms(cax);

hr = draw(rob, qd);
hr.Tag = [ hr.Tag , ' (Target)' ];

cax.View = [ -100 , +2.5 ];
viewtight(cax);

set( ...
    findobj( ...
        hr ...
      , 'type', 'patch' ...
    ) ...
  , 'FaceColor' , color('green') ...
  , 'EdgeColor' , color('MatteGreen') ...
);

% Set all text interpreters inside the figure to LaTeX
textinterpreters(ancestor(cax, 'figure'), 'latex');


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
