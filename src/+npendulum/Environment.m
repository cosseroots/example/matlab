classdef Environment < Environment
  %% ENVIRONMENT
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Environment(varargin)
      %% ENVIRONMENT
      
      
      obj@Environment();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
end
