classdef ComputedTorqueActuation < Actuation
  %% COMPUTEDTORQUEACTUATION
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Nontunable )
    
    % SMMS tree object
    SMMSTree
    
    % PID controller for generalized strain coordinates
    PID_q     (1,1) PIDControl  = PIDControl( ...
        'Kp', 1 ...
      , 'Ki', 1 ...
      , 'Kd', 1 ...
      , 'VelocityInput', true ...
    )
    
    % Time stamp after which to activate the controller
    TActivate (1,1) double = 1
    
    % Time span during which to transition the shape
    TTransition  (1,1) double = 5
    
    % Desired coordinates at [0, TTransition]
    QD        (:,2) double
    
    % Signal tracker
    Signals (1,1) SignalTracker
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = ComputedTorqueActuation(varargin)
      %% COMPUTEDTORQUEACTUATION
      
      
      
      obj@Actuation();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE MIXIN METHODS
  methods ( Access = protected )
    
    function setupImpl(obj, t, q, qdot)
      %% SETUPIMPL
      
      
      
      % Call parent's setup methods
      setupImpl@Actuation(obj, t, q, qdot);
      
      % Set sampling time of PID controller to that of the simulation step size
      obj.PID_q.Ts = obj.StepSize;
      
      % Signal tracker to track error
      obj.Signals = SignalTracker( ...
        'Name', { 'eq' , 'eqdot' , 'vpid' , 'tau' } ...
      );
      
    end
    
    
    function Qad = stepImpl(obj, t, q, qdot)
      %% STEPIMPL
      
      
      
      % Local variables for quicker access
      rob = obj.SMMSTree;
      
      % Time relative to when the controller is active
      trel = ( t - obj.TActivate ) / obj.TTransition;
      
      % Interpolate the desired strain coordinates
      qD = lerp( ...
          obj.QD(:,1) ...
        , obj.QD(:,2) ...
        , trel ...
      );
      
      % Interpolated velocities and accelerations
      qdotD  = 0 .* qD;
      qddotD = 0 .* qD;
      
      % Desired velocity if control is active
      if 0 <= trel && trel <= 1
        qdotD = ( obj.QD(:,2) - obj.QD(:,1) ) / obj.TTransition;
      end
      
      % Error signal and its time derivative
      e    = qD - q;
      edot = qdotD - qdot;
      vpid = step( ...
          obj.PID_q ...
        , e ...
        , edot ...
      );
      
      % Simple computed torque control law
      Qad = inverseDynamics( ...
          rob ...
        , q ...
        , qdot ...
        , qddotD + ...
          + vpid ...
      );
      
      step(obj.Signals, e, edot, vpid, Qad);
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      
      reset(obj.PID_q);
      
    end
    
    
    function releaseImpl(obj)
      %% RELEASEIMPL
      
      
      
      release(obj.PID_q);
      
    end
    
  end
  
end
