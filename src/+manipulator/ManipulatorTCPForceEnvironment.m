classdef ManipulatorTCPForceEnvironment < manipulator.Environment
  %% ENVIRONMENT
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Nontunable )
    
    % PID controller for generalized strain coordinates
    PID_q     (1,1) PIDControl  = PIDControl( ...
        'Kp', 1 ...
      , 'Ki', 1 ...
      , 'Kd', 1 ...
      , 'VelocityInput', true ...
    )
    
    % Time stamp after which to activate the controller
    TActivate (1,1) double = 1
    
    % Time span during which to transition the shape
    TTransition  (1,1) double = 5
    
    % Desired coordinates at [0, TTransition]
    QD        (:,2) double
    
    % Signal tracker
    Signals (1,1) SignalTracker
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = ManipulatorTCPForceEnvironment(varargin)
      %% ENVIRONMENT
      
      
      
      obj@manipulator.Environment();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE MIXIN METHODS
  methods ( Access = protected )
    
    function setupImpl(obj, t, q, qdot)
      %% SETUPIMPL
      
      
      
      % Call parent's setup methods
      setupImpl@manipulator.Environment(obj, t, q, qdot);
      
      % Set sampling time of PID controller to that of the simulation step size
      obj.PID_q.Ts = obj.StepSize;
      
      % Signal tracker to track error
      obj.Signals = SignalTracker( ...
        'Name', { 'eq' , 'eqdot' , 'ypid' , 'Ftcp' } ...
      );
      
    end
    
    
    function Fext = stepImpl(obj, t, q, qdot)
      %% STEPIMPL
      
      
      
      % Call parent step
      stepImpl@manipulator.Environment(obj, t, q, qdot);
      
      % Robot object
      rob = obj.SMMSTree;
      np = rob.PositionNumber;
      
      % Initialize variables
      e    = zeros(np, 1);
      edot = zeros(np, 1);
      vpid = zeros(np, 1);
      
      % Initialize external force
      Fext = zeros(6, 1);
      
      % Relative time wrt to controller's start moment
      trel = ( t - obj.TActivate ) / obj.TTransition;
      
      % Calculate external force only if controller is active
      if trel >= 0
        % Interpolate the desired strain coordinates
        qD = lerp( ...
            obj.QD(:,1) ...
          , obj.QD(:,2) ...
          , trel ...
        );
        
        % "Interpolated" velocities and accelerations
        qdotD  = ( obj.QD(:,2) - obj.QD(:,1) ) / obj.TTransition;
        qddotD = 0 .* qD;
        
        % Error signal and its time derivative
        e    =    qD - q;
        edot = qdotD - qdot;
        vpid = step( ...
            obj.PID_q ...
          , e ...
          , edot ...
        );
        
        % Simple computed torque control law
        Qad = inverseDynamics( ...
            rob ...
          , q ...
          , qdot ...
          , qddotD + ...
            + vpid ...
        );
        
%         % Calculate all body poses and their Jacobians
%         [T_, ~, ~, J_] = tangentReconstructKinematics( ...
%             rob ...
%           , q, [], [] ...
%           , eye(np, np), [], [] ...
%         );
        
        % Get TCP pose
        Ttcp = forwardKinematics(rob, q);
        
         Fext_ = zeros(6, 1);
        vFext_ = transformForce(eye(6, 6), Ttcp, rob.Ground.T);
        [Qad_, vQa_] = tangentInverseDynamics( ...
            rob ...
          ,  q, 0 * qdot, 0 * ( qddotD + vpid ), Fext_ ...
          , [], [], [], vFext_ ...
        );
        
        % Calculate TCP force needed to generate the desired joint actuation
        Ftcp = decomposition(Qa_) \ Qad_;
        
        % Express FTCP in base coordinates
        Fext = transformForce(Ftcp, Ttcp, rob.Ground.T);
        
      end
      
      % Track signals
      step(obj.Signals, e, edot, vpid, Fext);
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      
      reset(obj.PID_q);
      resetImpl@manipulator.Environment(obj);
      
    end
    
    
    function releaseImpl(obj)
      %% RELEASEIMPL
      
      
      
      release(obj.PID_q);
      releaseImpl@manipulator.Environment(obj);
      
    end
    
  end
  
end
