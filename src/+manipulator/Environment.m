classdef Environment < Environment
  %% ENVIRONMENT
  
  
  
  %% NONTUNABLE PROPERTIES
  properties ( Nontunable )
    
    % SMMS tree object for quicker execution
    SMMSTree
    
    % Frame rate of visualization. Set to 0 to disable
    VisualizationFPS (1, 1) double = 10
    
  end
  
  
  
  %% NONTUNABLE INTERNAL PROPERTIES
  properties ( Nontunable , Access = protected )
    
    Visualization
    VisualizationStepSize
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Environment(varargin)
      %% ENVIRONMENT
      
      
      
      obj@Environment();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE SYSTEM METHODS
  methods ( Access = protected )
    
    function setupImpl(obj, t, q, qdot)
      %% SETUPIMPL
      
      
      
      setupImpl@Environment(obj, t, q, qdot);
      
      if isempty(obj.SMMSTree)
        obj.SMMSTree = obj.Parent.SMMSTree;
      end
      
      if obj.VisualizationFPS > 0
        obj.VisualizationStepSize = 1 / obj.VisualizationFPS;
        obj.Visualization = smms.graphics.SMMSTreeChart( ....
            figure() ...
          , 'SMMSTree', obj.SMMSTree ...
          , 'Q'       , q ...
        );
        obj.Visualization.Title.Interpreter = 'latex';
        obj.Visualization.Title.String      = 'Initialization...';
        
      end
      
    end
    
    
    function fext = stepImpl(obj, t, q, qdot)
      %% STEPIMPL
      
      
      
      fext = stepImpl@Environment(obj, t, q, qdot);
      
      % Update visualization with 10fps i.e., all 100ms
      if ~isempty(obj.Visualization) && isvalid(obj.Visualization) ...
          && ceil(mod(t, obj.VisualizationStepSize) * 1000) < obj.StepSize * 1000
        obj.Visualization.Title.String = sprintf('Time $ %.3f / %.3f \\mathrm{s} $', t, obj.Parent.TimeSpan(2));
        draw(obj.Visualization, 'Q', q);
        drawnow('limitrate', 'nocallbacks');
        
      end
      
    end
    
    
    function releaseImpl(obj)
      %% RELEASEIMPL
      
      
      
      delete(obj.Visualization);
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      
      s = saveObjectImpl@matlab.System(obj);
      
      if isLocked(obj)
        s.VisualizationStepSize = obj.VisualizationStepSize;
        
      end
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      
      if wasInUse
        obj.VisualizationStepSize = s.VisualizationStepSize;
      end
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
end
