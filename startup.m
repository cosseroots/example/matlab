function startup()
%% STARTUP initializes this project



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-07-25
% Changelog:
%   2022-07-25
%       * Initial release



%% Fire up this project

% Add project's directories to MATLAB's search path
p = mtl_projpath();
addpath(p{:});


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header
