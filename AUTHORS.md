# Authors

This following list includes contributors how have directly touched code or documentation of COSSEROOTS.
It is not a complete list, in the sense that many others have influenced the development of COSSEROOTS through discussion, bug reports and more.

## Main Team

* Philipp T. Tempel <philipp.tempel@ls2n.fr> (core team/developer)

## Contributors
